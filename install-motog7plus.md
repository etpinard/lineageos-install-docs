# LineageOS 17.1 install instructions for a Motorola G7 Plus

### Phone specs

- Code name: `lake`
- Model number: XT1965-2
- LineageOS Wiki: https://wiki.lineageos.org/devices/lake

### Why this phone?

It is a decent mid-range phone (I got mine for 250 CAD) with solid LineageOS
support and decent coverage on my cell network. The _Plus_ version has a better
camera and a (very) slightly better chip. It is 2019 phone, but the equivalent
2020 G8 series is apparently not that much better. This phone has a headphone
jack and you can listen to music half-decently through its speakers.

Source:
- https://www.androidpolice.com/2020/05/15/guide-best-phones-tablets-lineageos/
- https://www.kimovil.com/en/frequency-checker/CA/motorola-moto-g7-plus
- https://www.gsmarena.com/motorola_moto_g7_plus-9533.php

### Install docs I found online

Full official installation procedure using Lineage's own recovery image:
- https://wiki.lineageos.org/devices/lake/install

Full installation procedure using TWRP recovery:
- https://www.getdroidtips.com/lineage-os-17-motorola-moto-g7-plus/
- https://www.androidauthority.com/lineageos-install-guide-893303/

To unlock bootloader (aka step 2)
- https://www.thecustomdroid.com/unlock-bootloader-on-moto/

To install TWRP Recovery and Root (aka step 3)
- https://www.thecustomdroid.com/install-twrp-root-moto-g7-plus-guide/

Nicely put-together install docs for a OnePlus6 where most concepts apply to
the Motorola G7 Plus
- https://www.codetinkerer.com/2019/08/31/install-lineageos-16-on-oneplus-6.html


## Step 0: Find a way to use an _old_ USB 2.0 port

This is important. The `fastboot` commands below will stale if your phone is
connected to a USB 3.0 port! So, you might (like me) have to use an old laptop
to install LineageOS.

For reference, I was on a t420s Thinkpad running Debian 10.


## Step 1: Install adb and fastboot

I ended up using the most latest versions of `adb` and `fastboot` downloaded
from the Google repos:

```
wget https://dl.google.com/android/repository/platform-tools-latest-linux.zip
unzip platform-tools-latest-linux.zip
```

You can install `android-tools-adb` and friends off some Linux' distro default
package manager, but it sounded like using the latest version was the preferred
way.

For reference,

```
./platform-tools/adb --version
Android Debug Bridge version 1.0.41
Version 30.0.4-6686687

./platform-tools/fastboot --version
fastboot version 30.0.4-6686687
```


## Step 2: Unlock bootloader

### (2a) Create a Motorola account

You will need this to unlock to OEM:

- Go to https://motorola-global-portal.custhelp.com/app/standalone/bootloader/unlock-your-device-a
- review risks, click next
- create a Motorola account
- leave that page open

### (2b) Setup stock Android phone

- Charge your phone to at least 60%
- Enable Developer mode on your phone by going to _About_ and tapping on the
  build number about 10 times
- Go in _System_ -> _Advanced_ -> _Developer options_ and then:
  + Enable OEM
  + Enable USB debugging

### (2c) Request unlock code

Plug in your phone and try:

```
./platform-tools/adb devices

# or (like on Debian 10)
sudo ./platform-tools/adb devices

# or try enabling file transfer off your phone's notification bar
# and then try above commands again

# you should see serial number in the command output
```

Then, run

```
./platform-tools/adb reboot bootloader
```

Once your phone is showing the bootloader screen:

```
./platform-tools/fastboot devices

# similarly here, you might have to sudo this command

# you should again see your phone's serial number

# then
./platform-tools/fastboot oem get_unlock_data
```

Warning: the UI on the Motorola page is not great:

- Copy that multi-line string (including the prefixes),
- use the formatter available on the Motorola page.
- Agree to the terms and conditions and then
- click on the _Request Unlock Key_ button at the bottom of the page

Motorola will then send you an email with unlock code.

### (2d) Use unlock code to unlock the bootloader

Open the email you received from Motorola, copy the unlock code and run:

```
./platform-tools/fastboot oem unlock UNLOCK_CODE

# then if your phone does not reboot automatically (like mine):
./platform-tools/fastboot reboot
```

A warning page saying that the bootloader is unlock should appear.


## Step 3: Install recovery

I tried to install Lineage's own recovery image found on
https://download.lineageos.org/lake, but after running:

```
./platform-tools/fastboot flash boot lineageos-17.1-20200908-recovery-lake.img
```

I could not get into the _Recovery Mode_ and follow the rest of the official
install instructions. I suspect my phone had some unconventional A/B slot setup.

So, I decided to install TWRP recovery following
https://www.thecustomdroid.com/install-twrp-root-moto-g7-plus-guide/ instead.
But as you'll see in steps 4-5, maybe I should have made a few more attempts
using the LineageOS recovery image.

According to thecustomdroid, after installing TWRP, you also **need** to install
Magisk to root the phone and thus to prevent an infinite boot loop out of the
TWRP recovery mode. So I went ahead and did that also.

### (3a) Push required files to phone's internal storage

- Grab the latest `.img` file from: https://dl.twrp.me/lake/
- Grab the latest Magisk zip file from: https://github.com/topjohnwu/Magisk/releases/download/v20.0/Magisk-v20.0.zip

N.B. If you're on the "normal" G7 (river), you may have to use
`copy-partitions-AB.zip` file as well as mentioned in the thecustomdroid
article.

Then,

```
./platform-tools/adb push twrp-3.4.0-0-lake.img sdcard
./platform-tools/adb push Magisk-v20.0.zip sdcard
```

### (3b) Install TWRP recovery

```
# reboot the bootloader
./platform-tools/adb reboot bootloader

# boot the recovery image
./platform-tools/fastboot boot twrp-3.4.0-0-lake.img
```

Your phone should then be in TWRP recovery mode!

Then, tap on _Advanced_ -> _Install Recovery Ramdisk_ and then navigate to
`sdcard` and select the TWRP `.img` file. Swipe to install!

Important: do not reboot your phone into the system yet. This (accoriding to
thecustomdroid) might trigger an infinite boot loop!

### (3c) Install Magisk

- Select the _Install_ button in TWRP recovery top-level menu
- Navigate to the directory where the Magisk installer zip file was transferred.
- Select the Magisk zip file
- Then swipe to install
- Finally, tap on the _Reboot System_ button to reboot your phone.


## Step 4: Install LineageOS 17.1

First, get the latest zip file over at: https://download.lineageos.org/lake

Then

```
# push it to your phone:
./platform-tools/adb push lineage-17.1-20200908-nightly-lake-signed.zip sdcard

# reboot the bootloader
./platform-tools/adb root bootloader

# use volume button to flip to "recovery"
# and then use power button to boot into TWRP recovery
```

In TWRP recovery,

- Go to _Advanced_ -> _Wipe_
- Select `system`, `data` and `cache` (this folder actually did not exist on my
  phone, possibly because it was a brand new phone, so I also wiped the `Dalvik
  cache` which might not have been a good idea).
- Then swipe to wipe!
- Back up and go to _Install_ of the top-level TWRP menu
- Select the LineageOS zip file, and swipe to install!

Then, things did not go according to plan:

- I went back to the TWRP top-level menu and then in `Mount` to mount `system`
  and `data` (that were wiped out before installing the LineageOS zip file) like
  the getdroidtips article suggested.
- After that, I went into _Reboot_ and tried to reboot the system, but TWRP
  printed a _No OS found_ warning. I tried to reboot anyway and I went into
  what appeared to be an infinite reboot loop.

## Step 5: How I got out of this mess

- I held down the Power and Volume down button to get into the bootloader
  (similar to `adb reboot bootloader`)
- Then, I tried to redo most of step 4, but to no luck.

So in desperation, I ran from the bootloader:

```
./platform-tools/fastboot flash boot lineageos-17.1-20200908-recovery-lake.img
```

again, as in step 2 ... and after a bit of a wait LineageOS (the actual
LineageOS, not its recovery) booted!

Not sure how that worked. It might be magic.

But hey, I got LineageOS working on my Motorola G7 Plus!

I hope these instructions (or blog post?) can help some people in their
degoogling adventures.
