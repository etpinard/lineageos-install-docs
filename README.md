# LineageOS 17.1 install instructions

## Degoogled Android basics

- https://lbry.tv/@RobBraxmanTech:6/de-googling-an-android-10-phone:a

## Explanation of why unlocking your phone's bootloader can be a security risk

- https://www.howtogeek.com/142502/htg-explains-the-security-risks-of-unlocking-your-android-phones-bootloader/

## Miscellaneous LineageOS content

- https://www.reddit.com/r/LineageOS/
- https://www.androidpolice.com/?s=Lineageos&order=rel
- https://forum.xda-developers.com/

## Table of content

- [LineageOS 17.1 install instructions for a Motorola G7 Plus](./install-motog7plus.md)
- [LineageOS 17.1 post-install: _Example LineageOS 17.1 post-install privacy-focused #degoogled configuration steps_](./post-install.md)
