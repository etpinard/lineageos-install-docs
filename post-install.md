# LineageOS 17.1 post-install

Example LineageOS 17.1 post-install privacy-focused `#degoogled` configuration
steps.

<div style="text-align:center"><img src="/assets/app-drawer.small.png"/></div>

_all the apps you need_

## App stores

To replace Google Play, install:

- F-Droid
    + Go to https://f-droid.org/, download the APK
    + Settings -> Security -> Unknown sources -> Allow
    + Open F-Droid from the app menu and complete the installation there
- Aurora Store
    + Install it directly from F-Droid
    + Before installing things, spoof your identity in Aurora Store settings

## Browsers

### Firefox

Install it from the Aurora Store. Even though the Aurora Store lists Firefox as
Google-services dependent (aka GSF-dependent), things appear to work smoothly as
of now (i.e. using version Firefox Daylight v81.1.1). I suspect Firefox only uses
GSF things to make their notifications better.

Note that one can install Firefox by downloading the APK found on
https://wiki.mozilla.org/Mobile/Platforms/Android - but currently this points to
the same version that Firefox Fennec found on F-Droid uses.

### DuckDuckGo

Install from F-Droid. It comes with a widget which can be nice for quick
DuckDuckGo searches from the home screen.

### Firefox Focus

Install from the Aurora Store. Unlike (plain) Firefox, Firefox Focus is not
GSF-dependent.

### Firefox Fennec

Install from F-Droid. Fully open-source.

### Tor

Install from the Aurora Store. Not GSF-dependent.

## Email Client

Install FairEmail using the APK found on https://github.com/M66B/FairEmail/releases

FairEmail will send you notifications whenever a new release is available.
FairEmail can also be installed via F-Droid, but new releases do not get pushed
there as often.

**N.B.** FairEmail (and any other non-Gmail clients) will not be able to sync
with a Gmail account unless you are signed into your Google account at the OS
level - which doing on a LineageOS phone would kind of defeat the purpose.  So,
if you need to check your Gmail, use a browser tab.

## Two-factor authentication

I recommend `Aegis` from F-Droid.

## Maps and navigation

Install [OsmAnd~](https://f-droid.org/fr/packages/net.osmand.plus/) and/or
[Organic Maps](https://f-droid.org/en/packages/app.organicmaps/) from F-Droid,
though most people will probably miss Google Maps here. Searching for `!maps
_location_` in DuckDuckGo could be an _ok_ replacement for some.

## Signal

Get the Signal APK from https://signal.org/android/apk/.

Note that Signal will warn you that without GSF it needs to run a more
resource-extensive background process in order to get message notifications to
work. But even with this background process running, my Motorola G7 Plus can get
me through more than two days without a charge.

More on the topic:

- https://blog.mossroy.fr/2020/06/27/signal-sur-lineageos-et-e/ (en français)
- https://forum.f-droid.org/t/ive-degoogled-signal-messenger/10443/12

## Other recommended apps

From F-Droid:

- Open Camera (to replace LineageOS' Camera app)
- VLC (as a from-storage music player)
- ImagePipe (picture editor)
- Termux (Linux terminal emulator)
- Pdf Viewer Plus
- QRCode & Barcode scanner
- Share to Clipboard
- Wikipedia

## In-browser apps

Could-be Android apps, but probably work just fine as browser tabs:

- weather apps
- food-delivery apps
- banking apps
